// JavaScript Document
var lastIndex;
var lastTab=0;
var menuLength = 0;
var Animating = true;
var Spacer = 40;
var ScrollTop = 0;
var margintop,scrolltop,template;
var contentsTabCount = 0;
var zbox = false;
var AllPages = '';
var megaStorage = new Array;
var megaHeight = new Array;
var megaMenu = new Array;
var megaTitle = new Array;
var megaTitleSpaced = new Array;
var Pages = new Array;
var tabString = new Array;
var tabContent = new Array;
var tabArray = new Array;
function initiateMinimalist(){
	
}
function initXml(xmlfile){
	$.get(xmlfile).done(function(data,textStatus){
		if(textStatus=='success'){
			var pages = $(data).find('pages');
			$(pages).find('page').each(function(index){
				megaTitleSpaced[index]=$(this).attr('name');
				megaTitle[index]='';
				var tempTitle = $(this).attr('name').split(" ");
				for(var i=0;i<tempTitle.length;i++){
					megaTitle[index]+=tempTitle[i];
				}
				$('#topmenu').append('<li class="topmenuitem"><a id="'+index+'" rel="address:/'+megaTitle[index]+'" title="'+megaTitleSpaced[index]+'">'+megaTitleSpaced[index]+'</a></li>');
				$('#footerleft .footerlist').append('<li><a rel="address:/'+megaTitle[index]+'" title="'+megaTitleSpaced[index]+'">'+megaTitleSpaced[index]+'</a></li>');
				
				var contents = $(this).find('contents').text();
				if(contents){
					$(this).find('contents').each(function(){
						var tabs;
						var contentType=$(this).attr('type');
						var contentName=$(this).attr('name');
						if(contentType=='text'||contentType=='Text'||contentType=='TEXT'||contentType=='txt'||contentType=='Txt'||contentType=='TXT'||contentType==''||contentType==null){
							if (!contentName){
								$(this).attr('name','');
							}
							$('#content').append('<div class="content"><ul class="breadmenu"><li class="breadmenuitem"><a rel="address:/" title="Zenor.net"><p id="homesprite"></p></a></li><li class="breadmenuitem selected"><a>'+$(this).attr('name')+'</a></li></ul><div class="contentbox">'+$(this).text()+'</div></div>');
						}else if(contentType=='columns'||contentType=='Columns'||contentType=='COLUMNS'||contentType=='cols'||contentType=='Cols'||contentType=='COLS'){
							var columnCount=$(this).find('column').length;
							$(this).find('column').each(function(){
								var columnTitle=$(this).attr('name');
								var columnIcon=$(this).attr('icon');
								var columnText=$(this).text();
								$('#content').append('<div class="tricontent"><ul class="breadmenu"><li class="trimenuitem"><a>'+columnTitle+'<p id="'+columnIcon+'"></p></a></li></ul><div class="contentbox">'+columnText+'</div></div>');
								var spacer=Spacer/2;
								$('.tricontent').width((100/columnCount)+'%');
								var space=$('.tricontent').width();
								$('.tricontent').css({'width':Math.ceil(space-spacer+(spacer/columnCount)),'margin-right':spacer});
								$('.tricontent:nth-child('+columnCount+'n)').addClass('last').css({'margin-right':0});
							});
						}else if(contentType=='tabset'||contentType=='Tabset'||contentType=='TabSet'||contentType=='TABSET'||contentType=='tabs'||contentType=='Tabs'||contentType=='TABS'){
							$('#content').append('<div class="tabset"><ul class="breadmenu"><li class="tabmenuitem"><a>'+megaTitleSpaced[index]+'</a></li></ul><div class="tabbox"></div></div>');
							var indexx = index;
							$(this).find('tab').each(function(index){
								var tabTitle=$(this).attr('name');
								tabString[index]='<li class="breadmenuitem"><a rel="address:/'+megaTitle[indexx]+'~'+(index+1)+'" title="'+tabTitle+'">'+tabTitle+'</a></li>';
								$('.breadmenu').append(tabString[index]);
								tabContent[index]=$(this).text();
							});
							tabArray[index] = tabContent;
							tabContent=[];
							
							tabs = '';
							for (var i=0;i<tabArray[index].length;i++){
								if (i!=0){
									tabs += '<div class="tab-'+i+'" style="display:none;">'+tabArray[index][i]+'</div>';
								} else {
									tabs += '<div class="tab-'+0+'" style="display:block;">'+tabArray[index][0]+'</div>';
								}
							}
							$('.tabbox').html(tabs);
							
							$('.tabset .breadmenu .breadmenuitem').eq(0).addClass('selected');
							lastTab = 1;
						}else if(contentType=='gallery'||contentType=='Gallery'||contentType=='GALLERY'||contentType=='grid'||contentType=='Grid'||contentType=='GRID'||contentType=='images'||contentType=='Images'||contentType=='IMAGES'||contentType=='imgs'||contentType=='Imgs'||contentType=='IMGS'){
							$('#content').append('<div class="gallery"><ul class="breadmenu"><li class="tabmenuitem"><a>'+megaTitleSpaced[index]+'</a></li></ul><div class="contentbox"><ul id="gallerylist"></ul></div></div>');
							var ind = index;
							
							var cols = parseInt( $(this).attr('columns') );
							var margins = (cols*2)*5;
							var gallerywidth = ($('#gallerylist').width() - margins) / cols ;
							$(this).find('image').each(function(index){
								var images = new Array();
								images[index] = $(this).attr('link');
								
								$('#gallerylist').append('<li class="gallerylistitem"><a class="block" rel="address:/'+megaTitle[ind]+'#'+images[index]+'"><img src="'+$(this).attr('thumbnail')+'"/></a></li>').css({});
								$('.gallerylistitem').css({'width':gallerywidth});
							});
							$('#gallerylist').append('<br class="clearfix" />');
						}
					});
					Pages[index] = '<div class="node-' + index + '">' + $('#content').html() + '</div>';
					$('#content').html('');
				}
				AllPages += Pages[index];
			});
			$('#content').html(AllPages);
			
			$('#menudialog').height('auto');
			initMegaMenu(pages);
			$('#menudialog').height(0);
		}
		renderContent(0);
	},"xml");
};
function initMegaMenu(pages){
	$(pages).find('page').each(function(index){
		var megamenu = $(this).find('megamenu').text();
		if(megamenu){
			megaStorage[index]='<h3><font color="#141414">//&nbsp;</font><a rel="address:/'+megaTitle[index]+'" title="'+megaTitleSpaced[index]+'">'+megaTitleSpaced[index]+'</a></h3>'+megamenu;
			
			$('#menudialog').html(megaStorage[index]);
			megaHeight[index] = $('#menudialog').height();
			$('#menudialog').html('');
		}else{
			megaStorage[index] = null;
			megaHeight[index] = 0;
		}
		menuLength++;
	});
	megaStorage[menuLength] = null;
	megaHeight[menuLength] = 0;
	megaMenu=[megaStorage,megaHeight];
	
	$('#header').mouseover(function(){
		openMegaMenu(menuLength);
	});
	$('#navigation').hover(function(){},function(){
		openMegaMenu(menuLength);
	});
	$('.topmenuitem').hover(function(){
		var i = $(this).index();
		openMegaMenu(i);
	});
	
	initiateMinimalist();
};
function openMegaMenu(i){
	if (i==menuLength||megaMenu[1][i]==0){
		if (Animating==true){
			$('#menudialog').stop(true).animate({
				'height':megaMenu[1][i],
				'margin-top':-1*megaMenu[1][i]
			},{
				complete: function(){
					$('#menudialog').html(megaMenu[0][i]);
				}
			},{
				queue: false,
				duration: 'slow'
			});
			Animating=false;
		}
	} else {
		$('#menudialog').html(megaMenu[0][i]);
		var linklistLength = $('#menudialog>ul.linklist').children('li').length;
		$('.linklist>li').width(99/linklistLength+'%');
		$('#menudialog').stop(true).animate({
			'height':megaMenu[1][i]+Spacer,
			'margin-top':-1*megaMenu[1][i]-Spacer
		},'fast');
		Animating=true;
	}
};
function renderTab(id,index){
		if (lastTab!=(index)){
			
			for (var i=0;i<tabArray[id].length;i++){
				if (i!=index){
					$('.node-'+id+' .tab-'+i).css({'display':'none'});
				} else {
					$('.node-'+id+' .tab-'+i).css({'display':'block'})
				}
			}
			
			$('.node-'+id+' .tabset .breadmenu .breadmenuitem').eq(lastTab).removeClass('selected');
			$('.node-'+id+' .tabset .breadmenu .breadmenuitem').eq(index).addClass('selected');
			//$('.tabbox').html(tabArray[id][index]);
			
			console.log(lastTab);
			
			lastTab = index;
		}
}
function renderContent(index){
	if(lastIndex!=index){
		//$('#content').html(Pages[index]);
		for (var i=0;i<menuLength;i++){
			if(i!=index){
				$('.node-'+i).css({'display':'none'});
			}else{
				$('.node-'+index).css({'display':'block'});
			}
		}
		var content = $('body').html();
		lastIndex=index;
		renderTab(index,0);
		//lastTab = 0;
		document.title = megaTitleSpaced[index]+' // Zenor.net';
	}
};
function enableImgBox(locArray){
	if (zbox==false){
		$('body').append('<div id="imgholder"><img src="'+locArray[1]+'" /></div><a rel="address:'+locArray[0]+'" id="imgbox"></a>');
		$('#imgbox').css({'opacity':'0'}).height($('body').height()).animate({'opacity':'.9'});
		
		var width,height,marginleft;
		$('#imgholder img').one('load',function(){
			$('#imgholder').css({'background-image':'none'});
			width = $(this).width();
			height = $(this).height();
			marginleft = -1*(width/2)-10;
			margintop = -1*(height/2)-10;
			$('#imgholder').css({
				'margin-top':margintop+ScrollTop
			}).stop(true).animate({
				'width':width,'margin-left':marginleft
			},{
				complete: function(){
					$(this).animate({
						'height':height
					},{
						complete: function(){
							$('#imgholder img').animate({'opacity':'1'},'fast');
							zbox = true;
						}
					});
				}
			});
		}).each(function(){
			if(this.complete)$(this).load();
		}).css({'opacity':'0'});
	}
}
$(document).ready(function(){
	initXml(xmlFile);
	
	$(document).scroll(function(){
		ScrollTop = $(document).scrollTop();
		//$('#imgholder').stop(true).animate({'margin-top':margintop+ScrollTop});
	});
	
	var headerh1 = $('#headerlink').html();
	$('#headerlink').html('').append('<a rel="address:/">'+headerh1+'</a>');
	var parsedId=0;
	$.address.init(function(event){
	}).change(function(event){
		splitEvent = event.value.split("/");
		if(splitEvent.length<3){
			tabsetsplit = splitEvent[1].split("~");
			parsedId = $('.topmenuitem a[rel^="address:/'+tabsetsplit[0]+'"]').attr('id');
			if (parsedId!=null){
				renderContent(parsedId);
				if(tabsetsplit.length>1){
					renderTab(parsedId,tabsetsplit[1]-1);
				}else{
					renderTab(parsedId,0);
				}
			}
		}
		imgsplit = event.value.split('#');
		if(imgsplit.length>1){
			document.body.style.overflow = 'hidden';
			enableImgBox(imgsplit);
		}else{
			document.body.style.overflow = 'inherit';
			zbox = false;
			$('#imgholder').remove();
			$('#imgbox').remove();
		}
	});
});